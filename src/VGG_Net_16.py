def main():
    import torch
    import torchvision 
    import torchvision.transforms as transforms



    transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))])

    trainset = torchvision.datasets.CIFAR10(root = "./data", train = True, download = True, transform = transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size = 4, shuffle = True, num_workers = 4)
    testset = torchvision.datasets.CIFAR10(root = "./data", train = False, download = True, transform = transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size = 4, shuffle = False, num_workers = 4)

    classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')



    import torch.nn as nn
    import torch.nn.functional as F 
    import matplotlib.pyplot as plt
    import numpy as np

    class Model(nn.Module):
        def __init__(self):
            super(Model,self).__init__() #32x32
            self.conv1_1 = nn.Conv2d(3,64,3, padding =1) #32x32x64
            self.conv1_2 = nn.Conv2d(64,64,3, padding =1) 
            self.pool = nn.MaxPool2d(2,2) #16x16x64
            self.conv2_1 = nn.Conv2d(64,128,3, padding =1)
            self.conv2_2 = nn.Conv2d(128,128,3, padding =1) #16x16x128
            self.pool = nn.MaxPool2d(2,2)  #8x8
            self.conv3_1 = nn.Conv2d(128,256,3, padding =1)
            self.conv3_2 = nn.Conv2d(256,256,3, padding =1)
            self.conv3_3 = nn.Conv2d(256,256,3, padding =1)
            self.pool = nn.MaxPool2d(2,2) #4x4
            self.conv4_1 = nn.Conv2d(256,512,3, padding =1)
            self.conv4_2 = nn.Conv2d(512,512,3, padding =1)
            self.conv4_3 = nn.Conv2d(512,512,3, padding =1)
            self.pool = nn.MaxPool2d(2,2)#2x2
            self.conv5_1 = nn.Conv2d(512,512,3, padding =1)
            self.conv5_2 = nn.Conv2d(512,512,3, padding =1)
            self.conv5_3 = nn.Conv2d(512,512,3, padding =1)
            self.pool = nn.MaxPool2d(2,2)#1x1x512
            self.fc1 = nn.Linear(512, 4096)
            self.fc2 = nn.Linear(4096, 4096)
            self.fc3 = nn.Linear(4096,10)
        def forward(self,x):
            x = F.relu(self.conv1_1(x))
            x = self.pool(F.relu(self.conv1_2(x)))

            x = F.relu(self.conv2_1(x))
            x = self.pool(F.relu(self.conv2_2(x)))

            x = F.relu(self.conv3_1(x))
            x = F.relu(self.conv3_2(x))
            x = self.pool(F.relu(self.conv3_3(x)))

            x = F.relu(self.conv4_1(x))
            x = F.relu(self.conv4_2(x))
            x = self.pool(F.relu(self.conv4_3(x)))

            x = F.relu(self.conv5_1(x))
            x = F.relu(self.conv5_2(x))
            x = self.pool(F.relu(self.conv5_3(x)))

            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = self.fc3(x)
            return x
        
    model = Model()
    
    import torch.optim as op 
    criterion = nn.CrossEntropyLoss()
    optimizer = op.SGD(model.parameters(), lr = 0.001, momentum= 0.9)

    for epoch in range(4):
        run_loss = 0
        for i,data in enumerate(trainloader,0):
            inputs, labels = data
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            run_loss += loss.item()
            if i%2000 == 1999:
                print((epoch+1),(i+1), run_loss/2000 )
                run_loss = 0
    
    print("done")
    
    correct = 0
    total = 0
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    print('Accuracy of the network: ',  100 * correct / total,"%")
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            outputs = model(images)
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            for i in range(4):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1

    
    for i in range(10):
        print('Accuracy of %5s : %2d %%' % (
            classes[i], 100 * class_correct[i] / class_total[i]))

if __name__ == '__main__':
    main()