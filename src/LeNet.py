def main():
    import torch
    import torchvision 
    import torchvision.transforms as transforms



    transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,0.5,0.5),(0.5,0.5,0.5))])

    trainset = torchvision.datasets.CIFAR10(root = "./data", train = True, download = True, transform = transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size = 4, shuffle = True, num_workers = 4)
    testset = torchvision.datasets.CIFAR10(root = "./data", train = False, download = True, transform = transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size = 4, shuffle = False, num_workers = 4)

    classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')



    import torch.nn as nn
    import torch.nn.functional as F 
    import matplotlib.pyplot as plt
    import numpy as np



    
    class Model(nn.Module):
        def __init__(self):
            super(Model,self).__init__()
            self.conv1 = nn.Conv2d(3,6,5)
            self.pool = nn.MaxPool2d(2,2)
            self.conv2 = nn.Conv2d(6,16,5)
            self.fc1 = nn.Linear(16*5*5,120)
            self.fc2 = nn.Linear(120, 84)
            self.fc3 = nn.Linear(84,10)
        def forward(self,x):
            x = self.pool(F.relu(self.conv1(x)))
            x = self.pool(F.relu(self.conv2(x)))
            x = x.view(-1,400)
            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = self.fc3(x)
            return x
        
    model = Model()
    
    import torch.optim as op 
    criterion = nn.CrossEntropyLoss()
    optimizer = op.SGD(model.parameters(), lr = 0.001, momentum= 0.9)



    """
    for epoch in range(4):
        run_loss = 0
        for i,data in enumerate(trainloader,0):
            inputs, labels = data
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            run_loss += loss.item()
            if i%2000 == 1999:
                print((epoch+1),(i+1), run_loss/2000 )
                run_loss = 0
    
    print("done")
    
    correct = 0
    total = 0
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    print('Accuracy of the network: ',  100 * correct / total,"%")
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            outputs = model(images)
            _, predicted = torch.max(outputs, 1)
            c = (predicted == labels).squeeze()
            for i in range(4):
                label = labels[i]
                class_correct[label] += c[i].item()
                class_total[label] += 1

    
    for i in range(10):
        print('Accuracy of %5s : %2d %%' % (
            classes[i], 100 * class_correct[i] / class_total[i]))
    """



    def matplotlib_imshow(img, one_channel=False):
        img = img / 2 + 0.5    
        npimg = img.numpy()
        plt.imshow(np.transpose(npimg, (1, 2, 0)))

    

    from torch.utils.tensorboard import SummaryWriter
    writer = SummaryWriter('runs/CIFAR10')
    
    dataiter = iter(trainloader)
    images, labels = dataiter.next()

    img_grid = torchvision.utils.make_grid(images)

    matplotlib_imshow(img_grid, one_channel=True)

    writer.add_image('FOur_images', img_grid)
    
    
    writer.add_graph(model, images)



    """
    def select_n_random(data, labels, n=100):

        assert len(data) == len(labels)

        perm = torch.randperm(len(data))
        return data[perm][:n], labels[perm][:n]

    images, labels = select_n_random(trainset.data, trainset.targets)

    class_labels = [classes[lab] for lab in labels]

    features = images.view(-1, 28 * 28)
    writer.add_embedding(features,
                    metadata=class_labels,
                    label_img=images.unsqueeze(1))
    writer.close()
    """



    def images_to_probs(model, images):

        output = model(images)

        _, preds_tensor = torch.max(output, 1)
        preds = np.squeeze(preds_tensor.numpy())
        return preds, [F.softmax(el, dim=0)[i].item() for i, el in zip(preds, output)]


    def plot_classes_preds(model, images, labels):
        preds, probs = images_to_probs(model, images)
        fig = plt.figure(figsize=(12, 48))
        for idx in np.arange(4):
            ax = fig.add_subplot(1, 4, idx+1, xticks=[], yticks=[])
            matplotlib_imshow(images[idx], one_channel=True)
            ax.set_title("{0}, {1:.1f}%\n(label: {2})".format(
                classes[preds[idx]],
                probs[idx] * 100.0,
                classes[labels[idx]]), color=("green" if preds[idx]==labels[idx].item() else "red"))
        return fig

    import torch.optim as op 
    criterion = nn.CrossEntropyLoss()
    optimizer = op.SGD(model.parameters(), lr = 0.001, momentum= 0.9)


    
    for epoch in range(4):
        run_loss = 0
        for i,data in enumerate(trainloader,0):
            inputs, labels = data
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            run_loss += loss.item()
            if i%1000 == 999:
                
                writer.add_scalar('training loss', run_loss / 1000, epoch * len(trainloader) + i)
                writer.add_figure('predictions vs. actuals', plot_classes_preds(model, inputs, labels), global_step=epoch * len(trainloader) + i)
                
                print((epoch+1),(i+1), run_loss/1000 )
                run_loss = 0.0
    
    print("done")

    class_probs = []
    class_preds = []
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            output = model(images)
            class_probs_batch = [F.softmax(el, dim=0) for el in output]
            _, class_preds_batch = torch.max(output, 1)

            class_probs.append(class_probs_batch)
            class_preds.append(class_preds_batch)

    test_probs = torch.cat([torch.stack(batch) for batch in class_probs])
    test_preds = torch.cat(class_preds)


    def add_pr_curve_tensorboard(class_index, test_probs, test_preds, global_step=0):

        tensorboard_preds = test_preds == class_index
        tensorboard_probs = test_probs[:, class_index]

        writer.add_pr_curve(classes[class_index],
                        tensorboard_preds,
                        tensorboard_probs,
                        global_step=global_step)
        writer.close()

    for i in range(len(classes)):
        add_pr_curve_tensorboard(i, test_probs, test_preds)





if __name__ == "__main__":
    main()
    





"""
1 2000 2.1820951613783834
1 4000 1.8470268206745386
1 6000 1.6385187877416612
1 8000 1.565793974712491
1 10000 1.5156327488422394
1 12000 1.4238116424158216
2 2000 1.364826869301498
2 4000 1.3489419441670178
2 6000 1.3163177466094493
2 8000 1.3004442430250347
2 10000 1.2834242822416126
2 12000 1.2840850218404085
3 2000 1.192818589322269
3 4000 1.2083297550193965
3 6000 1.185458666612394
3 8000 1.1909186975583435
3 10000 1.1868371532876045
3 12000 1.1698065574616194
4 2000 1.0955363985560835
4 4000 1.1132509931847454
4 6000 1.1071334371203556
4 8000 1.1037909479048102
4 10000 1.099535751858726
4 12000 1.0757084212768824
done
Accuracy of the network:  59.51 %
Accuracy of plane : 67 %
Accuracy of   car : 65 %
Accuracy of  bird : 57 %
Accuracy of   cat : 38 %
Accuracy of  deer : 43 %
Accuracy of   dog : 44 %
Accuracy of  frog : 79 %
Accuracy of horse : 61 %
Accuracy of  ship : 75 %
Accuracy of truck : 63 %




raceback (most recent call last):
  File "/home/long/Desktop/Pytorch/LeNet.py", line 194, in <module>
    main()
  File "/home/long/Desktop/Pytorch/LeNet.py", line 132, in main
    images, labels = select_n_random(trainset.data, trainset.targets)
  File "/home/long/Desktop/Pytorch/LeNet.py", line 130, in select_n_random
    return data[perm][:n], labels[perm][:n]
TypeError: only integer tensors of a single element can be converted to an index

Projector part 
"""